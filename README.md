**Clonefile** is a Ruby gem for cloning files on supported copy-on-write (COW) file systems.
[Learn more about file cloning](https://www.ctrl.blog/entry/file-cloning.html).

On Linux 5.7 and later, the gem performers the same operation as `cp --reflink` (using [`FICLONE`](https://www.man7.org/linux/man-pages/man2/ioctl_ficlone.2.html)). As of kernel 5.7, only Btrfs, OCFS2, and XFS supports `ficlone`.
This gem only supports creating reflinks to whole files and not ranges within a file.
However, it exports both the IOCTL [`ficlonerange`](https://www.man7.org/linux/man-pages/man2/ioctl_ficlonerange.2.html) and [`fideduprange`](https://www.man7.org/linux/man-pages/man2/ioctl_fideduperange.2.html) constants.

On macOS 10.12.1, the gem performs the same operation as `cp -c` (using [`clonefile`](https://opensource.apple.com/source/xnu/xnu-6153.81.5/bsd/man/man2/clonefile.2.auto.html)). As of macOS 11, only APFS supports `clonefile`.


## Get started

You can find build and install instructions in [BUILDING.md](BUILDING.md).

You probably want to install it via [RubyGems](https://rubygems.org/gems/clonefile):

```sh
gem install clonefile
```

## Usage

```Ruby
require 'clonefile'

# Clone the file or fallback to make a regular copy.
Clonefile.auto(source_path, destination_path)

# Clone the file or raise an exception.
Clonefile.always(source_path, destination_path)
```
