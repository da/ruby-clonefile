# frozen_string_literal: true

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

# Clones files on copy-on-write file systems.
module Clonefile
  class UnsupportedPlatform < RuntimeError
  end

  # Clones src to dest path. Returns boolean. Raises exception if unsupported.
  def self.always(src, dest)
    if RUBY_PLATFORM =~ /darwin/
      darwin_clonefile(src, dest)
    elsif RUBY_PLATFORM =~ /linux/
      begin
        src_fd = IO.sysopen(src, File::RDONLY | File::BINARY)
        dest_f = File.new(dest, File::WRONLY | File::BINARY | File::CREAT)
        res = dest_f.ioctl(LINUX_IOCTL_FICLONE, src_fd).zero?
      ensure
        (src_f = IO.new(src_fd)) && src_f.close
        dest_f.close
      end

      res
    else
      raise UnsupportedPlatform
    end
  end

  # Clones src to dest path. Falls back to a regular copy if unsupported.
  def self.auto(src, dest)
    always(src, dest)
  rescue UnsupportedPlatform, Errno::EBADF, Errno::EINVAL, Errno::EISDIR,
         Errno::ENOTSUP, Errno::EOPNOTSUPP, Errno::ETXTBSY, Errno::EXDEV
    FileUtils.cp(src, dest)
  end
end

require 'clonefile/clonefile'
