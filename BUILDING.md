To compile the C extension:

```sh
rake compile
```

To build and install the gem:

```sh
gem build clonefile.gemspec
gem install clonefile*.gem
```
