// Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
// SPDX-License-Identifier: BSD-3-Clause

#include "ruby.h"
#include "extconf.h"

VALUE rb_mod;

#ifdef __APPLE__

#include <sys/attr.h>
#include <sys/clonefile.h>

VALUE rb_clonefile(VALUE self, VALUE src, VALUE dest)
{
  if (RB_TYPE_P(src, T_STRING) != 1 || RB_TYPE_P(dest, T_STRING) != 1)
  {
    rb_raise(rb_eTypeError, "Arguments must be path strings.");
  }

  if (clonefile(RSTRING_PTR(src), RSTRING_PTR(dest), 0) == 0)
  {
    return Qtrue;
  }

  return Qfalse;
}

void Init_clonefile()
{
  rb_mod = rb_define_module("Clonefile");

  rb_define_module_function(rb_mod, "darwin_clonefile", rb_clonefile, 2);
}

#endif
#ifdef __linux__


#include <sys/ioctl.h>
#include <linux/fs.h>

void Init_clonefile()
{
  rb_mod = rb_define_module("Clonefile");

  rb_define_const(rb_mod, "LINUX_IOCTL_FICLONE", INT2FIX(FICLONE));
  rb_define_const(rb_mod, "LINUX_IOCTL_FICLONERANGE", INT2FIX(FICLONERANGE));
  rb_define_const(rb_mod, "LINUX_IOCTL_FIDEDUPERANGE", INT2FIX(FIDEDUPERANGE));
}

#endif
