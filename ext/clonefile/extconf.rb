# frozen_string_literal: true

require 'mkmf'

def dependency_header(header)
  abort "Unmet dependency: #{header}" unless have_header(header)
end

if RUBY_PLATFORM =~ /darwin/
  dependency_header('sys/attr.h')
  dependency_header('sys/clonefile.h')

elsif RUBY_PLATFORM =~ /linux/
  dependency_header('sys/ioctl.h')
  dependency_header('linux/fs.h')

else
  raise('Unsupported platform!')
end

create_header

create_makefile 'clonefile/clonefile'
