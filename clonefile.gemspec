# frozen_string_literal: false

# Copyright 2020 Daniel Aleksandersen <https://www.daniel.priv.no/>
# SPDX-License-Identifier: BSD-3-Clause

Gem::Specification.new do |s|
  s.name = 'clonefile'
  s.version = '0.5.3'
  s.date = '2020-12-18'
  s.summary = 'Clone files on copy-on-write file systems.'
  s.description = 'Copy files using reflink/cloned extents on supported file systems.'
  s.description << ' Supports Linux and macOS.'
  s.authors = 'Daniel Aleksandersen'
  s.email = 'code@daniel.priv.no'
  s.license = 'BSD-3-Clause'
  s.extra_rdoc_files = %w[LICENSES/BSD-3-Clause.txt]
  s.required_ruby_version = '>= 2.4'
  s.files = %w[lib/clonefile.rb ext/clonefile/clonefile.c]
  s.extensions = %w[ext/clonefile/extconf.rb]
  s.requirements = 'Linux 4.9.1+ or macOS 10.12.1+.'
  s.requirements << ' A copy-on-write file system.'
  s.metadata = {
    'bug_tracker_uri' => 'https://codeberg.org/da/ruby-clonefile/issues',
    'source_code_uri' => 'https://codeberg.org/da/ruby-clonefile'
  }
end
